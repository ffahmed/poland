﻿import unicodedata
import time
import random
import json
import math
from pprint import pprint
from collections import Counter
import operator
import sys
import requests;
from geopy.distance import vincenty
from geopy.distance import great_circle

inputPath = 'C:/Users/fiahmed/workplace/Mine/travel_guide/travelguide_workspace/TestProject/src/'
#sys.stdout = open(inputPath + 'stat.tsv', 'w')

## washington state
#boundary = {'north': 49.0024305, 'east': -116.91558, 
#            'south': 45.5485987, 'west': -124.7857167}

# california state
#boundary = {'north': 42.0095169, 'east': -114.1313926, 
#            'south': 32.5342643, 'west': -124.415165}

#boundary = {'north': 37.9749295, 'east': -122.4194155, 
#            'south': 37.7749295, 'west': -122.4194155}


ignoreList = ["grocery_or_supermarket", 
              "department_store", 
              "electronics_store", 
              "home_goods_store", 
              "clothing_store", 
              "furniture_store",
              "pharmacy",
              "real_estate_agency",
              "lodging",
              "lawyer",
              "finance",
              "gym",
              "parking",
              "travel_agency",
              "local_government_office",
              "insurance_agency",
              "hospital",
              "post_office",
              "general_contractor",
              "dentist",
              "school",
              "car_rental",
              "doctor",
              "establishment",
              "car_repair",
              "car_dealer",
              "electrician",
              "spa",
              "rv_park",
              "gas_station",
              "jewelry_store",
              "health",
              "airport",
              "fire_station",
              "cemetery",
              "church",
              "car_wash",
              "police",
              "storage",
              "courthouse",
              "accounting",
              "shoe_store",
              "bank",
              "hardware_store",
              "moving_company",
              "atm",
              "plumber",
              "premise",
              "funeral_home",
              "beauty_salon",
              "hair_care",
              "beauty_salon",
              "convenience_store",
              "veterinary_care",
              "liquor_store",
              "book_store",
              "physiotherapist",
              "florist",
              "bicycle_store",
              "meal_delivery",
              "store",
              "meal_takeaway",
              "transit_station",
              "bus_station",
              "train_station",
              "subway_station",
              "mosque",
              "synagogue",
              "movie_rental",
              "painter",
              "roofing_contractor",
              "movie_theater",
              "locksmith",
              "pet_store",
              "hindu_temple",
              "bakery",
              "shopping_mall",
              "university",
              "place_of_worship",
              "laundry",

              "library",
              "stadium",
              "food",
              "cafe",
              "bowling_alley",
              "embassy",
              
              "point_of_interest"
              ]


poisJson = ["seattle1",
            "la1", 
            "chicago1", 
            "chicagoAll", 
            "ny1", 
            "nyAll", 
            "wichita1", 
            "miami1", 
            "washingtondc1",
            "washingtondcAll",
            "redmond1",
            "redmondAll",
            "bellevue1",
            "bellevueAll",
            "sfo1",
            "sfoAll",]

radius = 2500

placeList = [
        "new york"
        #,"las vegas"
        #,"san francisco"
        #,"orlando"
        #,"los angeles"
        #,"miami"
        #,"washington dc"
        #,"boston"
        #,"chicago"
        #,"san diego"
        #,"yellowstone national park"
        #,"seattle"
        ##"maui"
        #,"honolulu"
        #,"grand canyon national park"
        #,"new orleans"
        #,"oahu"
        #,"key west"
        #,"hawaii"
        #,"yosemite national park"
        #,"philadelphia"
        #,"sedona"
        #,"atlanta"
        #,"Phoenix"
        #,"Denver"
        #,"houston"
        #,"portland"
        #,"nashville"
        #,"anahem"
        #,"napa"
        #,"salt lake city"
        #,"palm springs"
        #,"niagara falls"
        #,"tampa"
        #,"dallas"
        #,"memphis"
        #,"san antonio"
        #,"savannah"
        #,"ever glades"
        #,"lake tahoe"
        #,"austin"
        #,"death valley national park"
         ]

def frange(x, y, jump):
  while x <= y:
    yield x
    x += jump

def getBoundary(place):
    reqString = 'https://maps.googleapis.com/maps/api/geocode/json?&address='+str(place).replace(" ", "%20")
    resp = requests.get(reqString)
    if resp.status_code != 200:
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))

    data = json.loads(unicode(resp.content, "ISO-8859-1"))
    boundary = {}
    latLongField = 'bounds' if (data['results'][0]['geometry'].get('bounds')) else 'viewport'

    boundary['north'] = data['results'][0]['geometry'][latLongField]['northeast']['lat']
    boundary['east'] = data['results'][0]['geometry'][latLongField]['northeast']['lng']
    boundary['south'] = data['results'][0]['geometry'][latLongField]['southwest']['lat']
    boundary['west'] = data['results'][0]['geometry'][latLongField]['southwest']['lng']

    return boundary

def getNextpage(nextpagetoken):
    reqString = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken='+str(nextpagetoken)+'&key=AIzaSyCL-Dm8KHzDcftyyCYDdJqMr1J62nTk1JE'
    resp = requests.get(reqString)
    if resp.status_code != 200:
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))    
    return resp.content

def getNextPageToken(respJson):
    data = json.loads(unicode(respJson, "ISO-8859-1"))
    nextPageToken = data['next_page_token'] if (data.get('next_page_token')) else ""
    return nextPageToken

def getPoi(lat, long, radius):
    reqString = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location='+str(lat)+','+str(long)+'&radius='+str(radius)+'&type=point_of_interest&key=AIzaSyCL-Dm8KHzDcftyyCYDdJqMr1J62nTk1JE'
    resp = requests.get(reqString)
    if resp.status_code != 200:
        raise ApiError('GET /tasks/ {}'.format(resp.status_code))
    return resp.content


def poiTypeStat(input, allTypes = [], allRatingPerType = {}, avgRatingPerType = {}, frequencyPerType = {}, typesToPlaces = {}, isJSON=False):

    if (isJSON == False):
        text_file = open(input, "r")
        whole_thing = text_file.read()
    else:
        whole_thing = input

    #with open(input) as data_file:    
    #with open (infile, mode='r', buffering=-1) as data_file:
        #data = json.load(data_file)
    data = json.loads(unicode(whole_thing, "ISO-8859-1"))
    results = data['results']
    for i in range(len(results)):
        rating = results[i]['rating'] if results[i].get('rating') else "0"
        #print rating
        name = results[i]['name']
        #convert unicode to regular string
        name = unicodedata.normalize('NFKD', name).encode('ascii','ignore')

        types = results[i]['types']
        for iType in range(len(types)):
            type = types[iType]
            #print type
            allTypes.append(type)
        
            if (allRatingPerType.get(type) == None):
                allRatingPerType[type] = [float(rating)]
            else:
                allRatingPerType[type].append(float(rating))

            if (frequencyPerType.get(type) == None):
                frequencyPerType[type] = 0
            else:
                frequencyPerType[type] = frequencyPerType[type] + 1

            try:
                if (typesToPlaces.get(type) == None):
                    typesToPlaces[type] = [name + " " + str(rating)]
                else:
                    typesToPlaces[type].append(name + " " + str(rating))
            # ignore unicode error
            except UnicodeEncodeError:
                pass

    #for type, frequency in sorted_Frequency:
    #    if (type not in ignoreList):
    #        print type + " " + str(frequency) + " ",

    #print "\n"

    return  allTypes, allRatingPerType, avgRatingPerType, frequencyPerType, typesToPlaces

def printStats(allRatingPerType = {}, avgRatingPerType = {}, frequencyPerType = {}, filePointer=None):
    for type, ratings in allRatingPerType.iteritems():
        avgRatingPerType[type] = sum(ratings)/float(len(ratings))


    sorted_Rating = sorted(avgRatingPerType.items(), key=operator.itemgetter(1), reverse = True)
    sorted_Frequency = sorted(frequencyPerType.items(), key=operator.itemgetter(1), reverse = True)


    for type, rating in sorted_Rating:
        if (type not in ignoreList):
            print type + "\t" + str(rating)
            if (filePointer != None):
                filePointer.write(type + "\t" + str(rating) + "\n")


    print "\n"

def printTypesPlaces(allRatingPerType = {}, avgRatingPerType = {}, typesToPlaces={}, filePointer=None):
    for type, ratings in allRatingPerType.iteritems():
        avgRatingPerType[type] = sum(ratings)/float(len(ratings))


    sorted_Rating = sorted(avgRatingPerType.items(), key=operator.itemgetter(1), reverse = True)

    for type, rating in sorted_Rating:
        if (type not in ignoreList):
            if type in typesToPlaces:
                placeRatingList = typesToPlaces[type]
                s="\t"
                placeRatings = s.join(placeRatingList)
                print placeRatings
                if (filePointer != None):
                    filePointer.write(type + "\t" + placeRatings + "\n")

    print "\n"

def findCategoriesForPlace(boundary, radius, latLongStep, filePointer, filePointer2):

    allTypes = []
    allRatingPerType = {}
    avgRatingPerType = {}
    frequencyPerType = {}
    typesToPlaces = {}

    count = 0
    for lat in frange(boundary['south'], boundary['north'], latLongStep):            
        for long in frange(boundary['west'], boundary['east'], latLongStep):
            print 'processing ' + str(lat) + ', ' + str(long)
            count = count + 1
            respJson = getPoi(lat, long, radius)
            allTypes, allRatingPerType, avgRatingPerType, frequencyPerType, typesToPlaces = poiTypeStat(respJson, allTypes, allRatingPerType, avgRatingPerType, frequencyPerType, typesToPlaces, True)

            while True:
                nextPageToken = getNextPageToken(respJson)
                if (nextPageToken == ""):
                    break
                sleepTime = random.randrange(2,7)
                time.sleep(sleepTime)

                respJson = getNextpage(nextPageToken)
                allTypes, allRatingPerType, avgRatingPerType, frequencyPerType, typesToPlaces = poiTypeStat(respJson, allTypes, allRatingPerType, avgRatingPerType, frequencyPerType, typesToPlaces, True)

            sleepTime = random.randrange(2,7)
            time.sleep(sleepTime)
  
    print 'total points ' + str(count)
    printStats(allRatingPerType, avgRatingPerType, frequencyPerType, filePointer)
    printTypesPlaces(allRatingPerType, avgRatingPerType, typesToPlaces, filePointer2)


def main():

    ############## calculate scale ##################
    washington_ne = (49.0024305, -116.91558)
    washington_sw = (45.5485987, -124.7857167)
    lineDistance = math.sqrt(math.pow((49.0024305 - 45.5485987),2) + math.pow((-116.91558 - (-124.7857167)),2))
    print(lineDistance)
    distanceInKM = vincenty(washington_ne, washington_sw).kilometers
    LatLongPerKM = lineDistance/distanceInKM
    print LatLongPerKM
    latLongStep = (2.0*(radius*1.0/1000.0))*LatLongPerKM

    ################### iterate places ##############    
    for place in placeList:
        boundary = getBoundary(place)
        print "************" + str.upper(place) + "*************"
        print "North East: " + str(boundary['north']) + ", " + str(boundary['east'])
        print "South West: " + str(boundary['south']) + ", " + str(boundary['west'])
        f = open(place + ".categories", 'w')
        f2 = open(place + ".places", 'w')
        findCategoriesForPlace(boundary, radius, latLongStep, f, f2)
        f.close()
        f2.close()

if __name__ == "__main__":
    main()

    

