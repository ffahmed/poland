# # clustering dataset
# # determine k using elbow method
#  
# from sklearn.cluster import KMeans
# from sklearn import metrics
# from scipy.spatial.distance import cdist
# import numpy as np
# import matplotlib.pyplot as plt
#  
# 
# x1 = np.array([3, 1, 1, 2, 1, 6, 6, 6, 5, 6, 7, 8, 9, 8, 9, 9, 8])
# x2 = np.array([5, 4, 5, 6, 5, 8, 6, 7, 6, 7, 1, 2, 1, 2, 3, 2, 3])
#  
# plt.plot()
# plt.xlim([0, 10])
# plt.ylim([0, 10])
# plt.title('Dataset')
# plt.scatter(x1, x2)
# plt.show()
#  
# # create new plot and data
# plt.plot()
# X = np.array(list(zip(x1, x2))).reshape(len(x1), 2)
# colors = ['b', 'g', 'r']
# markers = ['o', 'v', 's']
#  
# # k means determine k
# distortions = []
# K = range(1,10)
# for k in K:
#     kmeanModel = KMeans(n_clusters=k).fit(X)
#     kmeanModel.fit(X)
#     distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
#  
# # Plot the elbow
# plt.plot(K, distortions, 'bx-')
# plt.xlabel('k')
# plt.ylabel('Distortion')
# plt.title('The Elbow Method showing the optimal k')
# plt.show()

# from sklearn.datasets.samples_generator import make_blobs
# batch_size = 45
# centers = [[1, 1], [-1, -1], [1, -1]]
# n_clusters = len(centers)
# X, labels_true = make_blobs(n_samples=3000, centers=centers, cluster_std=0.7)
# print ('done')
#  

import pandas as pd
import os

city = 'all'
num_of_clusters = 20
data_root = r'C:\Users\fiahmed\workplace\Mine\travel_guide\source\poland\NeuralNet\data\collected_pois\compare'

cluster_file = str(num_of_clusters) + '_clusters_' + city + '.csv'
top_file = 'top_50_' + city + '.csv'
out_file = os.path.join(data_root, 'top_poi_found_in_' + str(num_of_clusters) + '_clusters_' + city + '.csv')

def nameMatchesList(topList, name):
    for s in topList:
        if s.strip().lower().find(name.lower()) >=0 :
            return True
    return False

cluster = pd.read_csv(os.path.join(data_root, cluster_file), sep='\t')
top = pd.read_csv(os.path.join(data_root, top_file), sep='\t')
topList = top['name'].values
map(str.strip, topList)

f = open(out_file, 'w', encoding="utf-8")
for index, row in cluster.iterrows():
    if (row['cluster'] == 17):
        print ('found')
    if nameMatchesList(topList, row['name']):
        print (str(row['cluster']) + "\t" + row['name'])
        f.write(str(row['cluster']) + "\t" + row['name'] + "\n")
f.close()