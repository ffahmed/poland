import pandas as pd
import os
import numpy as np
from sklearn.cluster import KMeans
from scipy.spatial.distance import cdist
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA as sklearnPCA
from matplotlib import colors as mcolors


# Load data 
data_root = r'../data/collected_pois/temp_compare'
out_root = r'../data/collected_pois/temp_compare'
# train file
# train_file = 'poi_train.csv'
# train_file = 'poi_attributes_777.csv'
city = 'seattle'
train_file = 'poi_attributes_' + city + '.csv'
top_file = 'top_50_' + city + '.csv'

num_of_clusters = 20
maxK = 30
max_iteration = 300
elbow = False
out_file = os.path.join(out_root, str(num_of_clusters) + '_clusters_' + city + '.csv')
out_status_file = os.path.join(out_root, str(num_of_clusters) + '_iterations_' + city + '.csv')

def load_data_train_test():
    poi_train = pd.read_csv(os.path.join(data_root, train_file))
    poi_test = pd.read_csv(os.path.join(data_root, train_file))

    # process data and label
    poi_train_label = poi_train['label']
    poi_train = poi_train.drop('label',1)
    poi_train = poi_train.drop('poi_number',1)
    poi_test_label = poi_test['label']
    poi_test = poi_test.drop('label',1)
    poi_test = poi_test.drop('poi_number',1)
    return poi_train, poi_train_label, poi_test, poi_test_label

def load_data_clustering():
    pois = pd.read_csv(os.path.join(data_root, train_file))
    if 'label' in pois:
        pois = pois.drop('label', 1)
    pois = pois.replace(np.nan, '', regex=True)
    return pois
    

def evaluate(label_true, label_pred):
    from sklearn.metrics import mean_absolute_error
    # mean square
    print(np.mean((label_pred-label_true)**2))
    # mean absolute error
    print(mean_absolute_error(label_true, label_pred))

#########################################################
## Doc: http://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPRegressor.html
##########################################################
def try_nn_regressor(train, train_label, test, test_label):
    from sklearn.neural_network import MLPRegressor
    nn = MLPRegressor(solver='lbfgs', random_state=1, verbose=True, hidden_layer_sizes=(100,))
    print (nn.fit(train, train_label))
    test_pred = nn.predict(test)
    print (test_pred)
    evaluate(test_label, test_pred)

#########################################################
## Doc: http://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression
##########################################################
def try_linear_regression(train, train_label, test, test_label):
    from sklearn import linear_model
    regr = linear_model.LinearRegression()
    regr.fit(train, train_label)
    test_pred = regr.predict(test)    
    print (test_pred)
    evaluate(test_label, test_pred)
    print ('Variance Score: ' + str(regr.score(test, test_label))) 
    
#########################################################
## Doc: http://scikit-neuralnetwork.readthedocs.io/en/latest/index.html
##########################################################
def try_scikit_nn(train, train_label, test, test_label):
    from sknn.mlp import Regressor, Layer
    nn = Regressor(
        layers=[
            Layer("Rectifier", units=3),
            Layer("Linear")],
        learning_rate=0.00001,
        random_state = None,
        learning_rule = 'adagrad',
        n_iter=20)
    nn.fit(train, train_label)
    test_pred = nn.predict(test)    
    print (test_pred)
    df_test_pred = pd.DataFrame(test_pred, columns=['label'])        
    evaluate(test_label, df_test_pred['label'])
    
##########################################################
## Kmeans clustering given K
##########################################################    
def try_k_means(labels, X, k=2):
    kmeans = KMeans(n_clusters=k, random_state=0).fit(X)
#    print(kmeans.labels_)
#     print(kmeans.predict([[1,1,9,4092,1250,0.572],[47,1,7,5002,9794,0.593]]))
#     print(kmeans.cluster_centers_)
    
    # try plotting the clusters
    # try_plot_clustered_data_PCA(labels, X, kmeans.labels_, k)

    # try printing the clusters
    return print_cluster_poi_names(labels, kmeans.labels_)

######################################
# print poinames and cluster names
######################################
def print_cluster_poi_names(poi_names, poi_clusters):
    dict_cluster_names = {}
    dict_cluster_index = {}
    for i, name in enumerate(poi_names):
        cluster = poi_clusters[i]
        if cluster in dict_cluster_names:
            list = dict_cluster_names[cluster]
            list.append(name)
            listI = dict_cluster_index[cluster]
            listI.append(i)
        else:
            list = []
            list.append(name)
            dict_cluster_names[cluster] = list            
            listI = []
            listI.append(i)
            dict_cluster_index[cluster] = listI

    f = open(out_file, 'w', encoding="utf-8")
    f.write("cluster\tname\n")
    for key in dict_cluster_names.keys():
        list = dict_cluster_names[key]
        for name in list:
            try:
#                 print (str(key) + "\t" + str(name))
                f.write(str(key) + "\t" + str(name) + "\n")
            except:
                pass
#         print ('\n---------------\n')
    f.close()
    
    return dict_cluster_names, dict_cluster_index
############################################################
# Flot clustered data using PCA (dimensionality reduction)
#############################################################
def try_plot_clustered_data_PCA(labels, X, y, n_clusters):
    X_norm = (X - X.min())/(X.max() - X.min())
    pca = sklearnPCA(n_components=2) #2-dimensional PCA
    transformed = pd.DataFrame(pca.fit_transform(X_norm))
#     if we need more color option for more clusters
#     it = mcolors.CSS4_COLORS.keys().__iter__()
# it = mcolors.BASE_COLORS.keys().__iter__()
    # base colors only
    it = mcolors.CSS4_COLORS.keys().__iter__()
    for k in range(n_clusters):
        plt.scatter(transformed[y==k][0], transformed[y==k][1], label='Class' + str(k), c=it.__next__())
    
    
    for i, txt in enumerate(labels):
        plt.annotate(txt, (transformed[0][i],transformed[1][i]))
    
    plt.legend()
    plt.show()

##########################################################
## Elbow method to find best K
#########################################################
def try_elbow_method(X, maxK):
    # k means determine k
    distortions = []
    K = range(1,maxK)
    for k in K:
        print('processing ' + str(k))
        kmeanModel = KMeans(n_clusters=k).fit(X)
        kmeanModel.fit(X)
        distortions.append(sum(np.min(cdist(X, kmeanModel.cluster_centers_, 'euclidean'), axis=1)) / X.shape[0])
     
    # Plot the elbow
    plt.plot(K, distortions, 'ro-')
    plt.xlabel('K')
    plt.ylabel('Sum of squared error')
    plt.title('Finding correct K using elbow method')
    plt.savefig('elbow_method_K_SSE.pdf')
    plt.show()
    
#######################################################
# Only featurize bag of words into feature vector and return it.
#######################################################
def featurize_bag_of_words_only(data, text_column):
    from sklearn.feature_extraction.text import CountVectorizer
    data[text_column] = data[text_column].str.replace(':',' ')
    data_feature = data[text_column].values
    
    vectorizer = CountVectorizer()
    X = vectorizer.fit_transform(data_feature)
#     X
    X = X.toarray()
    
    # normalize
    X = normalize(X, 0, X.max())
    
#     np.savetxt(os.path.join(out_root, 'temparray.csv'), X, delimiter = ',')
    return X
   
def toFloat(value):
    if isinstance(value, list):
        return [toFloat(x) for x in value]
    else:
        try:
            return float(value)
        except ValueError:
            return 0

def normalize(value, minValue, maxValue):
    if isinstance(value, list):
        return [normalize(x, minValue, maxValue) for x in value]
    else:
        return (value - minValue) / (maxValue - minValue)

def auto_normalize(value):
    return normalize(value, min(value), max(value))

def add_dimensions(pois, poi_features):
    poi_features = np.column_stack((poi_features, normalize(toFloat(pois['GOOGLE_rating'].tolist()), 0, 5)))
    poi_features = np.column_stack((poi_features, auto_normalize(toFloat(pois['countReviews'].tolist()))))
    poi_features = np.column_stack((poi_features, auto_normalize(toFloat(pois['nearbyPoiCount'].tolist()))))
    poi_features = np.column_stack((poi_features, auto_normalize(toFloat(pois['nearbyCategoryCount'].tolist()))))
    return poi_features

def nameMatchesList(topList, name):
    for s in topList:
        if s.strip().lower().find(name.lower()) >=0 :
            return True
    return False

def prepareKmeansInput(pois, poi_indexes):
    new_pois = pois[pois.index.isin(poi_indexes)]
    new_pois = new_pois.reset_index(drop=True)
    return new_pois

def write_cluster_info(dict_cluster_names):
    f = open(out_file, 'w', encoding="utf-8")
    f.write("cluster\tname\n")
    for key in dict_cluster_names.keys():
        list = dict_cluster_names[key]
        for name in list:
            try:
                print (str(key) + "\t" + str(name))
                f.write(str(key) + "\t" + str(name) + "\n")
            except:
                pass
#         print ('\n---------------\n')
    f.close()

def printIterInfo(count_cluster_has_top, len_poi_index, poi_left_other_than_top_50, f, header = ''):
    print (header + '\n')
    f.write(header + '\n')
    aStr = 'top poi appears in ' + str(count_cluster_has_top) + ' clusters' + '\n'
    print (aStr)
    f.write(aStr)
    aStr = 'poi_left_other_than_top_50 ' + str(poi_left_other_than_top_50) + '\n'
    print (aStr)
    f.write(aStr)
    aStr = 'Number of pois left for next run ' + str(len_poi_index) + '\n'
    print (aStr)
    f.write(aStr)
        
    
# So maybe start with 500 POIs
# cluster them into 20 cluster
# Then pick the clusters (n) that contains top 50 (already identified) POIs
# Now pick all the POIs in n cluster and cluster them again into n
# And repeat the process multiple times.
# 
# At some point I would assume the top POIs are spread across all the clusters and stop at that point
# Now find the answer for the following
# 1) how many POIs left other than top 50 POI?
# 2) Is this top 50 POIs spread across all cluster?
# 3) If we discard a few of them, how many we have?
def run_custom_clustering_experiment():    
    top = pd.read_csv(os.path.join(data_root, top_file), sep='\t')
    topList = top['name'].values
    pois = load_data_clustering()

    f = open(out_status_file, 'w', encoding="utf-8")

    iteration = 0
    while True:        
        cluster_has_top = []
        count_cluster_has_top = 0
        poi_left_other_than_top_50 = 0

        pois_feature_values = featurize_bag_of_words_only(pois, 'reviews')
        pois_name_values = pois['poi_name'].values        
        dict_cluster_names, dict_cluster_index = try_k_means(pois_name_values, pois_feature_values, num_of_clusters)
    
        for key in dict_cluster_names.keys():
            list = dict_cluster_names[key]
            for name in list:
                try:
                    if nameMatchesList(topList, name):
    #                     print (str(row['cluster']) + "\t" + row['name'])                    
                        if (key not in cluster_has_top):
                            cluster_has_top.append(key)
                            count_cluster_has_top += 1
                    else:
                        poi_left_other_than_top_50 += 1
                except:
                    pass
                
        poi_indexes = []
        for cluster in cluster_has_top:
            poi_indexes.extend(dict_cluster_index[cluster])
        
        pois = prepareKmeansInput(pois, poi_indexes)
    
        printIterInfo(count_cluster_has_top, len(poi_indexes), poi_left_other_than_top_50, f)
                                    
        if iteration > max_iteration or count_cluster_has_top == 1 or count_cluster_has_top == num_of_clusters or len(poi_indexes) < num_of_clusters:
            print('TERMINATING CONDITION MET!')            
            f.write('TERMINATING CONDITION MET!')
            if (iteration > max_iteration):
                print('Max Iteration Reached')            
                f.write('Max Iteration Reached!')
            write_cluster_info(dict_cluster_names)
            break;
        
        iteration += 1
        
    f.close()
    
if __name__ == '__main__':
###################################################
# regression
###################################################
#     poi_train, poi_train_label, poi_test, poi_test_label = load_data_train_test() 
#     try_nn_regressor(poi_train, poi_train_label, poi_test, poi_test_label)
#     try_linear_regression(poi_train, poi_train_label, poi_test, poi_test_label)
#     try_scikit_nn(poi_train, poi_train_label, poi_test, poi_test_label)

##################################################
# clustering
##################################################
#     pois = load_data_clustering()
#     pois_feature_values = featurize_bag_of_words_only(pois, 'reviews')
#     if elbow == True:
#         try_elbow_method(pois_feature_values, pois_feature_values.shape[0] if (pois_feature_values.shape[0] < maxK) else maxK)
#     else:
#         try_k_means(pois['poi_name'].values, pois_feature_values, num_of_clusters)
    
#     maxK = 300
#     
#     pois_feature_values = add_dimensions(pois, pois_feature_values)
# 
#     try_k_means(pois['poi_name'].values, pois_feature_values, 20)

    run_custom_clustering_experiment();

    
